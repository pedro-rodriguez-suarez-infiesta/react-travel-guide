import { useEffect } from "react";
import { Link } from "react-router-dom";
/* import { toast } from "react-toastify"; */

import { API_HOME_URL } from "../constants/endpoints.js";

function Home({ locationList, setLocationList }) {
  useEffect(() => {
    const locationUrl = `${API_HOME_URL}`;
    fetch(locationUrl)
      .then((res) => res.json())
      .then((apiResponse) => {
        if (apiResponse.error) {
          throw new Error(apiResponse.error);
        }

        setLocationList(apiResponse);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [setLocationList]);

  /*  const locationUrl = `${API_URL}/${id} `;
      async function getLocation() {
        const apiResponse = await fetch(locationUrl);
        const json = await apiResponse.json();
        console.log(json);
        setLocation(apiResponse)
        
        
      }
      
      
      getLocation();
      
    },[id, location, setLocation]); */

  /* history.push("/"); */

  return (
    <div>
      <h1>This the home</h1>

      <div className="Home">
        <div className="Home__wrapper"></div>

        <div className="Home__description"></div>
        
        {(locationList || []).map((locationList) => (
          <Link to={`/location/${locationList.id}`} key={locationList.id}>
            <span key={locationList.cell} className="Home__cell">
              <h4 key={locationList.cellTitle} className="Home__cell--title">
                {locationList.name}
              </h4>
              <img
                key={locationList.images.id}
                src={[locationList.images.url]}
                alt="img"
                className="LocationList__img"
              ></img>
            </span>
          </Link>
        ))}
      </div>
    </div>
  );
}
export default Home;
