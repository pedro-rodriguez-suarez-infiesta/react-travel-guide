import { Link } from 'react-router-dom';

function NotFound() {
  return (
    <section>
      <h1>Not found!</h1> <Link to="/">Go home and find a better landscape </Link>
    </section>
  );
}

export default NotFound;
