import { Route, Switch } from "react-router-dom";
import { useState } from "react";

import "./App.scss";

import Header from "./components/Header";
import Home from "./pages/home";
import Location from "./pages/location";
import Footer from "./components/Footer";
import NotFound from "./pages/404";

function App() {
  const [location, setLocation] = useState(null);
  const [locationList , setLocationList]=useState([]);
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route path="/" exact>
          <Home locationList={locationList} setLocationList={setLocationList} />
        </Route>
        <Route path="/location/:id" exact>
          <Location location={location} setLocation={setLocation} />
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
