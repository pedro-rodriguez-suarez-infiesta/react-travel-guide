import { Link } from "react-router-dom";

import { useEffect } from "react";
import { API_HOME_URL } from "../../constants/endpoints";

function LocationList({locationList, setLocationList}){
    useEffect(() => {
        const locationUrl = `${API_HOME_URL}`;
        fetch(locationUrl)
          .then((res) => res.json())
          .then((apiResponse) => {
            if (apiResponse.error) {
              throw new Error(apiResponse.error);
            }
    
            setLocationList(apiResponse);
          })
          .catch((err) => {
            console.log(err);
          });
      }, [setLocationList]);
    return(

        <div className="List__wrapper">
            {locationList.map((locationList) => (
          <Link to={`/location/${locationList.id}`} key={locationList.id}>
            <span key={locationList.cell} className="List__cell">
              <h4 key={locationList.cellTitle} className="List__cell--title">
                {locationList.name}
              </h4>
              <img
                key={locationList.images.id}
                src={[locationList.images.url]}
                alt="img"
                className="LocationList__img"
              ></img>
            </span>
          </Link>
        ))}
        </div>
        
        
      


    )
}
export default LocationList