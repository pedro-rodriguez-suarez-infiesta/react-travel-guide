import { useEffect } from "react";
import { useHistory, useParams, Link } from "react-router-dom";
/* import { toast } from "react-toastify"; */

import { API_LOCATION_URL } from "../constants/endpoints";

import '../styles/location.scss'

function Location({ location, setLocation }) {
  const { id } = useParams();
  const history = useHistory();

  useEffect(() => {
    const locationUrl = `${API_LOCATION_URL}/${id}`;
    fetch(locationUrl)
      .then((res) => res.json())
      .then((apiResponse) => {
        if (apiResponse.error) {
          throw new Error(apiResponse.error);
        }

        setLocation(apiResponse);
      })
      .catch((err) => {
        console.log(err);

        // Con history push puedo enviar al usuario a la ruta que quiera
        /* history.push('/characters'); */
      });
  }, [id, setLocation]);

  /*  const locationUrl = `${API_URL}/${id} `;
    async function getLocation() {
      const apiResponse = await fetch(locationUrl);
      const json = await apiResponse.json();
      console.log(json);
      setLocation(apiResponse)
      
      
    }
    
    
    getLocation();
    
  },[id, location, setLocation]); */

  /* history.push("/"); */

  return (
    <div>
      <h1>This is a Hidden Location </h1>

      <div className="Location">
        {location ? (
          <>
            <h1>{location.name}</h1>

            <button
              onClick={() => {
                // Así volvemos atrás en el router
                history.goBack();
              }}
            >
              Go back
            </button>

            <Link to="/">Go home</Link>
            <div className="Location__wrapper"></div>

            
            <div className="Location__description"></div>
            {/* {location.description.map((description)=> (
                
                <span {description.rating} className="Location__rating">rating</span>
              ))} */}
            {(location.images||[]).map((images) => (
              <img
                key={images.id}
                src={[images.url]}
                alt="img"
                className="Location__img"
              ></img>
            ))}
            <h3>Description</h3>
            <ul className="Location__description--list"> What to do:
              {(location.description||[]).map((description) => (
                <>
                  <li className="Location__description--item" key={description}>
                  {description.main}
                    {description.sports}
                    {description.architecture} {description.nature}
                    {description.food}
                  </li>
                </>
              ))}
              
            </ul>
            <ul className="Location__comments--list">
              {(location.comments || []).map((comment) => (
                <>
                  <li
                    className="Location__comment"
                    key={[comment.id]}
                  >
                    {[comment.comment]}
                  </li>
                </>
              ))}
            </ul>
          </>
        ) : (
          // spinner
          <h2>Loading...</h2>
        )}
      </div>
    </div>
  );
}
export default Location;
